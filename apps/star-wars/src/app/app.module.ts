import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { environment, StateModule } from '@star-wars/sw-web/shared/core';
import { SwWebShellModule } from '@star-wars/sw-web/shell';
import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { SwWebPlanetDataAccessModule } from '@star-wars/sw-web/planet/data-access';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    SwWebShellModule,
    SwWebPlanetDataAccessModule,
    RouterModule,
    StateModule,
    HttpClientModule,
    StoreDevtoolsModule.instrument({ maxAge: 25, logOnly: environment.production })
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
