module.exports = {
  name: 'sw-web-planet-data-access',
  preset: '../../../../jest.config.js',
  coverageDirectory: '../../../../coverage/libs/sw-web/planet/data-access',
  snapshotSerializers: [
    'jest-preset-angular/AngularSnapshotSerializer.js',
    'jest-preset-angular/HTMLCommentSerializer.js'
  ]
};
