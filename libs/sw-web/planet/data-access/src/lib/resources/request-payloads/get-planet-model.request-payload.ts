export interface GetPlanetModelRequestPayload {
  id: string | number;
}
