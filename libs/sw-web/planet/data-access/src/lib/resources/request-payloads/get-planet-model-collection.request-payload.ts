export interface GetPlanetModelCollectionRequestPayload {
  page: number;
}
