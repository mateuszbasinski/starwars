import { async, TestBed } from '@angular/core/testing';
import { SwWebPlanetDataAccessModule } from './sw-web-planet-data-access.module';

describe('SwWebPlanetDataAccessModule', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [SwWebPlanetDataAccessModule]
    }).compileComponents();
  }));

  it('should create', () => {
    expect(SwWebPlanetDataAccessModule).toBeDefined();
  });
});
