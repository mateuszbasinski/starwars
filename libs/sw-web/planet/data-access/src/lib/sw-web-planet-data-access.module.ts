import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PlanetFacade } from './+state/planet.facade';
import { StoreModule } from '@ngrx/store';
import { PLANET_FEATURE_KEY, planetReducer } from './+state/planet.reducer';
import { PlanetDataMapperService } from './services/planet-data-mapper.service';
import { EffectsModule } from '@ngrx/effects';
import { PlanetEffects } from './+state/planet.effects';

@NgModule({
  imports: [
    CommonModule,
    StoreModule.forFeature(PLANET_FEATURE_KEY, planetReducer),
    EffectsModule.forRoot([PlanetEffects])
  ],
  providers: [
    PlanetFacade,
    PlanetDataMapperService
  ]
})
export class SwWebPlanetDataAccessModule {
}
