import { Injectable } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { PlanetPartialState } from './planet.reducer';
import { planetQuery } from './planet.selectors';
import * as fromPlanetActions from './planet.actions';
import { GetPlanetModelRequestPayload } from '../resources/request-payloads/get-planet-model.request-payload';
import { GetPlanetModelCollectionRequestPayload } from '../resources/request-payloads/get-planet-model-collection.request-payload';

@Injectable()
export class PlanetFacade {
  planetModel$ = this.store.pipe(select(planetQuery.getPlanetModel));
  planetModelLoading$ = this.store.pipe(
    select(planetQuery.getPlanetModelLoading)
  );
  planetModelLoadError$ = this.store.pipe(
    select(planetQuery.getPlanetModelLoadError)
  );
  planetModelCollection$ = this.store.pipe(
    select(planetQuery.getPlanetModelCollection)
  );
  planetModelCollectionLoading$ = this.store.pipe(
    select(planetQuery.getPlanetModelCollectionLoading)
  );
  planetModelCollectionLoadError$ = this.store.pipe(
    select(planetQuery.getPlanetModelCollectionLoadError)
  );

  constructor(private store: Store<PlanetPartialState>) {
  }

  getPlanetModel(data: GetPlanetModelRequestPayload): void {
    this.store.dispatch(new fromPlanetActions.GetPlanetModel(data));
  }

  getPlanetModelCollection(data: GetPlanetModelCollectionRequestPayload): void {
    this.store.dispatch(new fromPlanetActions.GetPlanetModelCollection(data));
  }
}
