import { createFeatureSelector, createSelector } from '@ngrx/store';
import { PLANET_FEATURE_KEY, PlanetState } from './planet.reducer';

// Lookup the 'Planet' feature state managed by NgRx
const getPlanetState = createFeatureSelector<PlanetState>(PLANET_FEATURE_KEY);

const getPlanetModel = createSelector(
  getPlanetState,
  state => state.planetModel
);

const getPlanetModelLoading = createSelector(
  getPlanetState,
  state => state.planetModelLoading
);

const getPlanetModelLoadError = createSelector(
  getPlanetState,
  state => state.planetModelLoadError
);

const getPlanetModelCollection = createSelector(
  getPlanetState,
  (state: PlanetState) => {
    return state.planetModelCollection;
  }
);

const getPlanetModelCollectionLoading = createSelector(
  getPlanetState,
  state => state.planetModelCollectionLoading
);

const getPlanetModelCollectionLoadError = createSelector(
  getPlanetState,
  state => state.planetModelCollectionLoadError
);

export const planetQuery = {
  getPlanetModel,
  getPlanetModelLoading,
  getPlanetModelLoadError,
  getPlanetModelCollection,
  getPlanetModelCollectionLoading,
  getPlanetModelCollectionLoadError
};
