import * as fromPlanetActions from './planet.actions';
import { HttpErrorResponse } from '@angular/common/http';
import { PlanetModel, PlanetResult } from '@star-wars/sw-web/planet/domain';

export const PLANET_FEATURE_KEY = 'planet';

export interface PlanetState {
  planetModel: PlanetModel | null;
  planetModelLoading: boolean;
  planetModelLoadError: HttpErrorResponse | null;
  planetModelCollection: PlanetResult;
  planetModelCollectionLoading: boolean;
  planetModelCollectionLoadError: HttpErrorResponse | null;
}

export interface PlanetPartialState {
  readonly [PLANET_FEATURE_KEY]: PlanetState;
}

export const initialState: PlanetState = {
  planetModel: null,
  planetModelLoading: false,
  planetModelLoadError: null,
  planetModelCollection: null,
  planetModelCollectionLoading: false,
  planetModelCollectionLoadError: null
};

export function planetReducer(
  state: PlanetState = initialState,
  action: fromPlanetActions.CollectiveType
): PlanetState {
  switch (action.type) {
    case fromPlanetActions.Types.GetPlanetModel: {
      state = {
        ...state,
        planetModel: null,
        planetModelLoading: true,
        planetModelLoadError: null
      };
      break;
    }

    case fromPlanetActions.Types.GetPlanetModelFail: {
      state = {
        ...state,
        planetModel: null,
        planetModelLoading: false,
        planetModelLoadError: action.payload
      };
      break;
    }

    case fromPlanetActions.Types.GetPlanetModelSuccess: {
      state = {
        ...state,
        planetModel: action.payload,
        planetModelLoading: false,
        planetModelLoadError: null
      };
      break;
    }

    case fromPlanetActions.Types.GetPlanetModelCollection: {
      state = {
        ...state,
        planetModelCollection: null,
        planetModelCollectionLoading: true,
        planetModelCollectionLoadError: null
      };
      break;
    }

    case fromPlanetActions.Types.GetPlanetModelCollectionFail: {
      state = {
        ...state,
        planetModelCollection: null,
        planetModelCollectionLoading: false,
        planetModelCollectionLoadError: action.payload
      };
      break;
    }

    case fromPlanetActions.Types.GetPlanetModelCollectionSuccess: {
      state = {
        ...state,
        planetModelCollection: action.payload,
        planetModelCollectionLoading: false,
        planetModelCollectionLoadError: null
      };
      break;
    }
  }
  return state;
}
