import { Action } from '@ngrx/store';
import { HttpErrorResponse } from '@angular/common/http';
import { GetPlanetModelRequestPayload } from '../resources/request-payloads/get-planet-model.request-payload';
import { PlanetModel, PlanetResult } from '@star-wars/sw-web/planet/domain';
import { GetPlanetModelCollectionRequestPayload } from '../resources/request-payloads/get-planet-model-collection.request-payload';

export enum Types {
  GetPlanetModel = '[Planet] Get Planet Model',
  GetPlanetModelFail = '[Planet] Get Planet Model Fail',
  GetPlanetModelSuccess = '[Planet] Get Planet Model Success',
  GetPlanetModelCollection = '[Planet] Get Planet Model Collection',
  GetPlanetModelCollectionFail = '[Planet] Get Planet Model Collection Fail',
  GetPlanetModelCollectionSuccess = '[Planet] Get Planet Model Collection Success'
}

export class GetPlanetModel implements Action {
  readonly type = Types.GetPlanetModel;

  constructor(public payload: GetPlanetModelRequestPayload) {
  }
}

export class GetPlanetModelFail implements Action {
  readonly type = Types.GetPlanetModelFail;

  constructor(public payload: HttpErrorResponse) {
  }
}

export class GetPlanetModelSuccess implements Action {
  readonly type = Types.GetPlanetModelSuccess;

  constructor(public payload: PlanetModel) {
  }
}

export class GetPlanetModelCollection implements Action {
  readonly type = Types.GetPlanetModelCollection;

  constructor(public payload: GetPlanetModelCollectionRequestPayload) {
  }
}

export class GetPlanetModelCollectionFail implements Action {
  readonly type = Types.GetPlanetModelCollectionFail;

  constructor(public payload: HttpErrorResponse) {
  }
}

export class GetPlanetModelCollectionSuccess implements Action {
  readonly type = Types.GetPlanetModelCollectionSuccess;

  constructor(public payload: PlanetResult) {
  }
}

export type CollectiveType =
  | GetPlanetModel
  | GetPlanetModelFail
  | GetPlanetModelSuccess
  | GetPlanetModelCollection
  | GetPlanetModelCollectionFail
  | GetPlanetModelCollectionSuccess;

