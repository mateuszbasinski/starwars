import { Injectable } from '@angular/core';
import { Effect } from '@ngrx/effects';
import { DataPersistence } from '@nrwl/angular';
import * as fromPlanetActions from './planet.actions';
import { PlanetPartialState } from './planet.reducer';
import { HttpErrorResponse } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { PlanetDataService } from '../services/planet-data.service';

@Injectable()
export class PlanetEffects {
  @Effect()
  getPlanetModel$ = this.dp.fetch(fromPlanetActions.Types.GetPlanetModel, {
    run: (action: fromPlanetActions.GetPlanetModel) => {
      return this.planetDataService
        .getPlanetModel(action.payload)
        .pipe(map(data => new fromPlanetActions.GetPlanetModelSuccess(data)));
    },
    onError: (
      action: fromPlanetActions.GetPlanetModel,
      error: HttpErrorResponse
    ) => {
      return new fromPlanetActions.GetPlanetModelFail(error);
    }
  });

  @Effect()
  getPlanetModelCollection$ = this.dp.fetch(fromPlanetActions.Types.GetPlanetModelCollection, {
    run: (action: fromPlanetActions.GetPlanetModelCollection) => {
      return this.planetDataService
        .getPlanetCollection(action.payload.page)
        .pipe(map(data => new fromPlanetActions.GetPlanetModelCollectionSuccess(data)));
    },
    onError: (
      action: fromPlanetActions.GetPlanetModelCollection,
      error: HttpErrorResponse
    ) => {
      return new fromPlanetActions.GetPlanetModelCollectionFail(error);
    }
  });

  constructor(
    private dp: DataPersistence<PlanetPartialState>,
    private planetDataService: PlanetDataService
  ) {
  }
}
