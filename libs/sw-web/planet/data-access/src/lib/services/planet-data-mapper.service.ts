import { Injectable } from '@angular/core';
import { PlanetApiResult, PlanetResult } from '@star-wars/sw-web/planet/domain';
import { Observable } from 'rxjs';
import { map} from 'rxjs/operators';
import { SwWebPlanetDataAccessModule } from '@star-wars/sw-web/planet/data-access';

@Injectable({
  providedIn: SwWebPlanetDataAccessModule
})
export class PlanetDataMapperService {

  mapper(data$: Observable<PlanetApiResult>): Observable<PlanetResult> {
    return data$.pipe(
      map(planets => ({
          count: planets.count,
          next: planets.next,
          previous: planets.previous,
          results: planets.results.map(
            planet => ({
              name: planet.name,
              rotationPeriod: planet.rotation_period,
              orbitalPeriod: planet.orbital_period,
              diameter: planet.diameter,
              climate: planet.climate,
              gravity: planet.gravity,
              terrain: planet.terrain,
              surfaceWater: planet.surface_water,
              population: planet.population,
              created: planet.created,
              edited: planet.edited,
              url: planet.url,
              residents: planet.residents,
              films: planet.films
            })
          )
        })
      )
    );
  }
}
