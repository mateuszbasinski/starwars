import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { GetPlanetModelRequestPayload } from '../resources/request-payloads/get-planet-model.request-payload';
import { apiEndpoints, PlanetApiResult, PlanetModel, PlanetResult } from '@star-wars/sw-web/planet/domain';
import { GetPlanetModelCollectionRequestPayload } from '../resources/request-payloads/get-planet-model-collection.request-payload';
import { PlanetDataMapperService } from './planet-data-mapper.service';

@Injectable({
  providedIn: 'root'
})
export class PlanetDataService {

  constructor(private http: HttpClient, private planetDataMapperService: PlanetDataMapperService) {
  }

  getPlanetCollection(pageId: number): Observable<PlanetResult> {
    const result$ = this.http.get<PlanetApiResult>(apiEndpoints.planets.getPlanets.format(String(pageId)));

    return this.planetDataMapperService.mapper(result$);
  }

  getPlanetModel(payload: GetPlanetModelRequestPayload): Observable<PlanetModel> {
    return this.http.get<PlanetModel>(apiEndpoints.planets.getPlanetModel);
  }

  getPlanetModelCollection(payload: GetPlanetModelCollectionRequestPayload): Observable<PlanetModel[]> {
    return this.http.get<PlanetModel[]>(apiEndpoints.planets.getPlanetModelCollection);
  }
}
