import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PlanetListFeatureRoutingModule } from './planet-list-feature-routing.module';
import { PlanetListContainerComponent } from './containers/planet-list-container/planet-list-container.component';
import { PlanetListComponent } from './components/planet-list/planet-list.component';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatButtonModule } from '@angular/material/button';
import { MatTableModule } from '@angular/material/table';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatIconModule } from '@angular/material/icon';

@NgModule({
  declarations: [
    PlanetListComponent,
    PlanetListContainerComponent
  ],
  imports: [
    CommonModule,
    PlanetListFeatureRoutingModule,
    MatToolbarModule,
    MatButtonModule,
    MatTableModule,
    MatPaginatorModule,
    MatFormFieldModule,
    MatInputModule,
    MatIconModule
  ]
})
export class PlanetListFeatureModule {
}
