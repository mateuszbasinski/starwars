import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output,
  ViewChild
} from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { PlanetModel, PlanetResult } from '@star-wars/sw-web/planet/domain';

@Component({
  selector: 'sw-planet-list',
  templateUrl: './planet-list.component.html',
  styleUrls: ['./planet-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PlanetListComponent {
  @Input()
  set planetsList(planetsList: PlanetResult) {
    if (planetsList) {
      this.dataSource = new MatTableDataSource(planetsList.results);
      this.dataSource.sort = this.sort;
      this.length = planetsList.count;
    }
  }

  @Output() changePageEmit = new EventEmitter<number>();
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  displayedColumns: string[] = ['name', 'rotationPeriod', 'orbitalPeriod', 'action'];
  dataSource: MatTableDataSource<PlanetModel>;
  pageNo = 0;
  length: number;
  pageSize = 10;

  applyFilter(event: Event): void {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  pageEvents(event: any): void {
    if (event.pageIndex > this.pageNo) {
      this.pageNo++;
    } else {
      this.pageNo--;
    }
    this.changePageEmit.emit(this.pageNo + 1);
  }
}
