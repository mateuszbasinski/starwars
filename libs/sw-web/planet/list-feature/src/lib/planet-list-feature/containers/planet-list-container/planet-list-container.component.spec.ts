import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PlanetListContainerComponent } from './planet-list-container.component';

describe('PlanetListContainerComponent', () => {
  let component: PlanetListContainerComponent;
  let fixture: ComponentFixture<PlanetListContainerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PlanetListContainerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlanetListContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
