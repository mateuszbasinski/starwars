import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { PlanetFacade } from '@star-wars/sw-web/planet/data-access';
import { PlanetResult } from '@star-wars/sw-web/planet/domain';

@Component({
  selector: 'sw-planet-list-container',
  templateUrl: './planet-list-container.component.html',
  styleUrls: ['./planet-list-container.component.scss']
})
export class PlanetListContainerComponent implements OnInit {
  planets$: Observable<PlanetResult>;

  constructor(
    private facade: PlanetFacade
  ) {
  }

  ngOnInit(): void {
    this.facade.getPlanetModelCollection({ page: 1 });
    this.planets$ = this.facade.planetModelCollection$;
  }

  getPage(pageId: number): void {
    this.facade.getPlanetModelCollection({page: pageId});
  }
}
