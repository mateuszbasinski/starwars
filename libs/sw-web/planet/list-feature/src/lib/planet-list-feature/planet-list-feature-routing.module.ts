import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PlanetListContainerComponent } from './containers/planet-list-container/planet-list-container.component';

const routes: Routes = [
  { path: '', component: PlanetListContainerComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PlanetListFeatureRoutingModule {
}
