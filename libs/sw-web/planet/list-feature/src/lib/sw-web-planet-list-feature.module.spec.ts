import { async, TestBed } from '@angular/core/testing';
import { SwWebPlanetListFeatureModule } from './sw-web-planet-list-feature.module';

describe('SwWebPlanetListFeatureModule', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [SwWebPlanetListFeatureModule]
    }).compileComponents();
  }));

  it('should create', () => {
    expect(SwWebPlanetListFeatureModule).toBeDefined();
  });
});
