module.exports = {
  name: 'sw-web-planet-list-feature',
  preset: '../../../../jest.config.js',
  coverageDirectory: '../../../../coverage/libs/sw-web/planet/list-feature',
  snapshotSerializers: [
    'jest-preset-angular/AngularSnapshotSerializer.js',
    'jest-preset-angular/HTMLCommentSerializer.js'
  ]
};
