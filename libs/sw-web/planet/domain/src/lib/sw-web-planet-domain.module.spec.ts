import { async, TestBed } from '@angular/core/testing';
import { SwWebPlanetDomainModule } from './sw-web-planet-domain.module';

describe('SwWebPlanetDomainModule', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [SwWebPlanetDomainModule]
    }).compileComponents();
  }));

  it('should create', () => {
    expect(SwWebPlanetDomainModule).toBeDefined();
  });
});
