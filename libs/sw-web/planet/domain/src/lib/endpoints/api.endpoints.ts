import { environment } from '@star-wars/sw-web/shared/core';

export const apiEndpoints = {
  planets: {
    getPlanets: `${environment.apiUrl}/planets/?page={0}`,
    getPlanetModel: `${environment.apiUrl}/`,
    getPlanetModelCollection: `${environment.apiUrl}/`
  }
};
