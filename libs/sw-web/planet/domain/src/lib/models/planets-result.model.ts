import { PlanetModel } from './planet.model';

export interface PlanetResult {
  count: number;
  next: string;
  previous: string;
  results: PlanetModel[];
}
