import { PlanetApiModel } from './planet-api.model';

export interface PlanetApiResult {
  count: number;
  next: string;
  previous: string;
  results: PlanetApiModel[];
}
