module.exports = {
  name: 'sw-web-planet-domain',
  preset: '../../../../jest.config.js',
  coverageDirectory: '../../../../coverage/libs/sw-web/planet/domain',
  snapshotSerializers: [
    'jest-preset-angular/AngularSnapshotSerializer.js',
    'jest-preset-angular/HTMLCommentSerializer.js'
  ]
};
