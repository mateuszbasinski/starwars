module.exports = {
  name: 'sw-web-planet-shell',
  preset: '../../../../jest.config.js',
  coverageDirectory: '../../../../coverage/libs/sw-web/planet/shell',
  snapshotSerializers: [
    'jest-preset-angular/AngularSnapshotSerializer.js',
    'jest-preset-angular/HTMLCommentSerializer.js'
  ]
};
