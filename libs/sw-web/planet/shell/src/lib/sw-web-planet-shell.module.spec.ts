import { async, TestBed } from '@angular/core/testing';
import { SwWebPlanetShellModule } from './sw-web-planet-shell.module';

describe('SwWebPlanetShellModule', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [SwWebPlanetShellModule]
    }).compileComponents();
  }));

  it('should create', () => {
    expect(SwWebPlanetShellModule).toBeDefined();
  });
});
