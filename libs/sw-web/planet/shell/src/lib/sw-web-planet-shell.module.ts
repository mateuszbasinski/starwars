import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PlanetListFeatureModule } from '@star-wars/sw-web/planet/list-feature';
import { SwWebPlanetRoutingModule } from './sw-web-planet-routing.module';

@NgModule({
  imports: [
    CommonModule,
    PlanetListFeatureModule,
    SwWebPlanetRoutingModule
  ]
})
export class SwWebPlanetShellModule {}
