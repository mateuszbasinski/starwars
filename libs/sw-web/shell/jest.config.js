module.exports = {
  name: 'sw-web-shell',
  preset: '../../../jest.config.js',
  coverageDirectory: '../../../coverage/libs/sw-web/shell',
  snapshotSerializers: [
    'jest-preset-angular/AngularSnapshotSerializer.js',
    'jest-preset-angular/HTMLCommentSerializer.js'
  ]
};
