import { async, TestBed } from '@angular/core/testing';
import { SwWebShellModule } from './sw-web-shell.module';

describe('SwWebShellModule', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [SwWebShellModule]
    }).compileComponents();
  }));

  it('should create', () => {
    expect(SwWebShellModule).toBeDefined();
  });
});
