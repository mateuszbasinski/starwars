import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SwWebPlanetRoutingModule } from './sw-web-shell-routing.module';
import { PlanetListFeatureModule } from '@star-wars/sw-web/planet/list-feature';

@NgModule({
  imports: [
    CommonModule,
    SwWebPlanetRoutingModule,
    PlanetListFeatureModule
  ]
})
export class SwWebShellModule {}
