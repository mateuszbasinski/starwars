module.exports = {
  name: 'sw-web-shared-core',
  preset: '../../../../jest.config.js',
  coverageDirectory: '../../../../coverage/libs/sw-web/shared/core',
  snapshotSerializers: [
    'jest-preset-angular/AngularSnapshotSerializer.js',
    'jest-preset-angular/HTMLCommentSerializer.js'
  ]
};
