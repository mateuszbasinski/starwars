import { ActionReducer, MetaReducer, StoreModule } from '@ngrx/store';
import { NgModule } from '@angular/core';
import { NxModule } from '@nrwl/angular';
import { localStorageSync } from 'ngrx-store-localstorage';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { environment } from './enviroments/environment';

const metaReducers: Array<MetaReducer<any, any>> = [localStorageSyncReducer];

export function localStorageSyncReducer(
  reducer: ActionReducer<any>
): ActionReducer<any> {
  return localStorageSync({ keys: ['tasks'], rehydrate: true })(reducer);
}

@NgModule({
  imports: [
    NxModule.forRoot(),
    StoreModule.forRoot(
      {},
      { metaReducers }
    ),
    !environment.production ? StoreDevtoolsModule.instrument() : []
  ]
})
export class StateModule {
}
