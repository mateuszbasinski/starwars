import { async, TestBed } from '@angular/core/testing';
import { SwWebSharedCoreModule } from './sw-web-shared-core.module';

describe('SwWebSharedCoreModule', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [SwWebSharedCoreModule]
    }).compileComponents();
  }));

  it('should create', () => {
    expect(SwWebSharedCoreModule).toBeDefined();
  });
});
